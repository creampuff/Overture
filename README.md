Overture 
-----------
Swift core library for Creampuff Applications



### APIs
| Name | Status |
|:------:|:------:|
| File      | Failed |
| Archiver | Failed |
| HTML Parser | Failed |
| JSON Parser | Failed |
| yaml Parser | Planning |
| Database ( Realm ) | Planning |
| 
 

### Platforms
| Platforms | Status |
|:------:|:------:|
| Creampuff (Linux) | Failed |
| OSX | Failed |
| iOS | Failed |
| BSD | Failed |
|  | Planning |
| Database ( Realm ) | Planning |
