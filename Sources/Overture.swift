// Overture v0.0.1
// Copyright (c) 2016 Shota Shimazu
// This program is freely distributed under the Apache license, see LICENSE for detail.
// 
// Platform support
// Overture is mainly developed for Creampuff OS (Linux). 
// OSX is also support as long as Linux support completed.
// Other Unix-like operating system such as FreeBSD, Solaris may be able to use this library. 
// But, Windows don't 